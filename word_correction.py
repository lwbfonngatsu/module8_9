import operator


warehouse = {
    (1, 1): "เฟบหวสุตหว่อ 123/789",
    (1, 2): "อุ๋งน้อยก้อยข้าวปุ้น 123/789",
    (1, 3): "ปังปวย 456/753",
    (2, 1): "ฬอลลันดื้ว 984/157",
    (2, 2): "ฮอววันคื้อ 984/157",
    (2, 3): "Null",
    (3, 1): "Null",
    (3, 2): "Null",
    (3, 3): "Null",
    (3, 4): "Null",
    (4, 1): "Null",
    (4, 2): "Null",
    (4, 3): "Null",
    (4, 4): "Null"
}


def word_correction(word):
    global warehouse
    input_len = len(word)
    corrects = {}
    for slot in warehouse:
        word_len = len(warehouse[slot])
        n_correct = 0
        if input_len == word_len:
            i = 0
            for char in word:
                if char == warehouse[slot][i]:
                    n_correct += 1
                i += 1
        else:
            # For case that OCR work uncorrected.
            pass

        corrects[slot] = n_correct / input_len
    cls = max(corrects.items(), key=operator.itemgetter(1))
    return cls


if __name__ == "__main__":
    pass
