import Module_OCR as ocr
import cv2
import categories
import utilities as utils
import configs.module_config as config
import numpy as np
from keras.models import model_from_json
import operator


def ensemble(results, threshold=0, select="all"):
    dct = {}
    for result in results:
        result = results[result][0]
        for i, conf in enumerate(result):
            if i not in dct:
                dct[i] = conf
            else:
                dct[i] += conf
    for key in dct:
        dct[key] = dct[key] / len(results)
    if threshold == 0:
        cls = max(dct.items(), key=operator.itemgetter(1))
    else:
        if abs(dct[0] - dct[1]) < threshold:
            cls = [0]
        else:
            cls = max(dct.items(), key=operator.itemgetter(1))
    return cls


class Model:
    def __init__(self):
        self.upp_models = {}
        self.mid_models = {}
        self.num_models = {}
        self.low_models = {}
        self.load()

    def load(self):
        for i in config.upper_models:
            with open('Models/CNN/upper_model/Model_v{0}/arch.json'.format(i), 'r') as f:
                self.upp_models["model{}".format(i)] = model_from_json(f.read())
            self.upp_models["model{}".format(i)].load_weights('Models/CNN/upper_model/Model_v{0}/model.hdf5'.format(i))

        for i in config.mid_models:
            with open('Models/CNN/mid_model/Model_v{0}/arch.json'.format(i), 'r') as f:
                self.mid_models["model{}".format(i)] = model_from_json(f.read())
            self.mid_models["model{}".format(i)].load_weights('Models/CNN/mid_model/Model_v{0}/model.hdf5'.format(i))

        for i in config.num_models:
            with open('Models/CNN/num_model/Model_v{0}/arch.json'.format(i), 'r') as f:
                self.num_models["model{}".format(i)] = model_from_json(f.read())
            self.num_models["model{}".format(i)].load_weights('Models/CNN/num_model/Model_v{0}/model.hdf5'.format(i))

        for i in config.lower_models:
            with open('Models/CNN/lower_model/Model_v{0}/arch.json'.format(i), 'r') as f:
                self.low_models["model{}".format(i)] = model_from_json(f.read())
            self.low_models["model{}".format(i)].load_weights('Models/CNN/lower_model/Model_v{0}/model.hdf5'.format(i))
        print("Loaded all models into memories.")

    def predict_upper(self, img):
        clss = {}
        for i in config.upper_models:
            model = self.upp_models["model{}".format(i)]
            resized = cv2.resize(img, (model.layers[0].input_shape[2], model.layers[0].input_shape[1]))
            img_s = np.expand_dims(resized, axis=0)
            clss["model{}".format(i)] = model.predict(img_s)
        cls = ensemble(clss)
        return cls

    def predict_mid(self, img):
        clss = {}
        for i in config.mid_models:
            model = self.mid_models["model{}".format(i)]
            resized = cv2.resize(img, (model.layers[0].input_shape[2], model.layers[0].input_shape[1]))
            img_s = np.expand_dims(resized, axis=0)
            clss["model{}".format(i)] = model.predict(img_s)
        cls = ensemble(clss)
        return cls

    def predict_num(self, img):
        clss = {}
        for i in config.num_models:
            model = self.num_models["model{}".format(i)]
            resized = cv2.resize(img, (model.layers[0].input_shape[2], model.layers[0].input_shape[1]))
            img_s = np.expand_dims(resized, axis=0)
            clss["model{}".format(i)] = model.predict(img_s)
        cls = ensemble(clss)
        return cls

    def predict_lower(self, img):
        clss = {}
        for i in config.lower_models:
            model = self.low_models["model{}".format(i)]
            resized = cv2.resize(img, (model.layers[0].input_shape[2], model.layers[0].input_shape[1]))
            img_s = np.expand_dims(resized, axis=0)
            clss["model{}".format(i)] = model.predict(img_s)
        cls = ensemble(clss)
        return cls

    def integizer(self, img):
        clss = {}
        for i in config.lower_models:
            model = self.low_models["model{}".format(i)]
            resized = cv2.resize(img, (model.layers[0].input_shape[2], model.layers[0].input_shape[1]))
            img_s = np.expand_dims(resized, axis=0)
            clss["model{}".format(i)] = model.predict(img_s)
        cls = ensemble(clss)
        return cls

    def main(self, cropped):
        result = ""
        room_num = ""
        name = ""
        dct = dict()
        cropped = cv2.resize(cropped, (640, 440))
        cropped[: 5, : cropped.shape[1]] = 0
        cropped[cropped.shape[0] - 5: cropped.shape[0], : cropped.shape[1]] = 0

        cropped[: cropped.shape[0], : 5] = 0
        cropped[: cropped.shape[0], cropped.shape[1] - 5:] = 0
        kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
        cropped = cv2.filter2D(cropped, -1, kernel)
        gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        _, threshed = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
        erode = cv2.morphologyEx(threshed, cv2.MORPH_ERODE, np.ones((9, 9), np.uint8))
        _, contours, _ = cv2.findContours(erode, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if contours:
            c = max(contours, key=cv2.contourArea)
            x, y, w, h = cv2.boundingRect(c)
            cropped = cropped[y: y + h, x: x + w]
        cropped = cv2.bilateralFilter(cropped, 5, 75, 75)
        lst_bnd, image_list, row_list, mid_level, top_level, bot_level = ocr.cha_segment(cropped)
        group_data = ocr.grouping_word(top_level, mid_level, bot_level)
        for n_line, data in enumerate(group_data):
            for n_group, keys in enumerate(data):
                for key in keys:
                    if key in mid_level[n_line]:
                        model = "integizer"
                        line = mid_level
                    elif key in top_level[n_line]:
                        model = "top"
                        line = top_level
                    else:
                        model = "bot"
                        line = bot_level
                    x1 = line[n_line][key][0] + row_list[n_line][0]
                    y1 = line[n_line][key][1] + row_list[n_line][1]
                    x2 = line[n_line][key][2] + row_list[n_line][0]
                    y2 = line[n_line][key][3] + row_list[n_line][1]
                    h = abs(y1 - y2)
                    w = abs(x1 - x2)
                    pre_img = cropped[y1: y1 + h, x1: x1 + w]
                    mod_img = utils.preprocess(pre_img)

                    if model == "integizer":
                        con = self.integizer(mod_img)
                        if con[0] == 0:
                            cls = self.predict_num(mod_img)
                            cls = config.num_label[config.num_cls[cls[0]]]
                        else:
                            cls = self.predict_mid(mod_img)
                            cls = config.mid_label[config.mid_cls[cls[0]]]
                        if n_line == 1 and n_group == 0:
                            cls = self.predict_num(mod_img)
                            cls = config.num_label[config.num_cls[cls[0]]]
                            room_num += cls
                        elif n_line == 0:
                            cls = self.predict_mid(mod_img)
                            cls = config.mid_label[config.mid_cls[cls[0]]]
                            name += cls
                    elif model == "top":
                        cls = self.predict_upper(mod_img)
                        cls = config.upp_label[config.upp_cls[cls[0]]]
                    else:
                        cls = self.predict_lower(mod_img)
                        cls = config.low_label[config.low_cls[cls[0]]]
                    result += cls
                result += " "

        dct["name"] = name
        dct["room"] = room_num
        return dct
