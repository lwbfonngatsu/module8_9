import cv2
import os
import numpy as np
from collections import namedtuple
import pytesseract
import imutils
import collections
import warnings

pytesseract.pytesseract.tesseract_cmd = "C:/Program Files (x86)/Tesseract-OCR/tesseract"

warnings.filterwarnings('ignore')
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def crop_frame_picture(image):
    x = 0
    y = 0
    height, width, _ = image.shape
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    blur = cv2.GaussianBlur(thresh, (5, 5), 0)
    edged = cv2.Canny(blur, 75, 200)
    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        if len(approx) == 4:
            if ((approx[2][0][0] - approx[0][0][0]) >= ((width / 100) * 60) and (approx[2][0][1] - approx[0][0][1]) >= (
                    (height / 100) * 60)):
                x1 = approx[0][0][0] + 7
                y1 = approx[0][0][1] + 7
                x2 = approx[1][0][0] + 7
                y2 = approx[1][0][1] - 7
                x3 = approx[2][0][0] - 7
                y3 = approx[2][0][1] - 7
                x4 = approx[3][0][0] - 7
                y4 = approx[3][0][1] + 7
                screenCnt = np.array([[[x1, y1]], [[x2, y2]], [[x3, y3]], [[x4, y4]]])
                image = image[screenCnt[0][0][1]:screenCnt[2][0][1], screenCnt[0][0][0]:screenCnt[2][0][0]]
                x = x1
                y = y1
                return image, x, y
            else:
                return image, x, y
        else:
            return image, x, y


def repeatly(data):
    c = collections.Counter(data)
    c = c.most_common(3)
    c_max = c[0]
    c_max_data = c[0][1]
    for i in range(len(c)):
        if c_max_data < c[i][1]:
            c_max = c[i]
            c_max_data = c[i][1]
    return c_max[0]


def area_mser(a, b):
    dx = min(a.xmax, b.xmax) - max(a.xmin, b.xmin)
    dy = min(a.ymax, b.ymax) - max(a.ymin, b.ymin)
    if (dx >= 0) and (dy >= 0):
        return None
    else:
        return b.xmin, b.ymin, b.xmax, b.ymax


def mean_mser(data):
    count = 1
    all_data = 0
    for i in data:
        all_data += i
        count += 1
    avr = all_data / count
    return avr


def tesseract_seg(image, array=np.array([])):
    h, w, _ = image.shape
    boxes = pytesseract.image_to_boxes(image, lang='tha')
    num_count = 0
    x_d = []
    y_d = []
    w_d = []
    h_d = []
    for b in boxes.splitlines():
        b = b.split(' ')
        xmin = int(b[1])
        ymax = h - int(b[2])
        xmax = int(b[3])
        ymin = h - int(b[4])
        x_d.append(xmin)
        y_d.append(ymin)
        w_d.append(xmax - xmin)
        h_d.append(ymax - ymin)
        num_count += 1
    if len(w_d) >= 3 and len(h_d) >= 3:
        m_w, m_h = repeatly(w_d), repeatly(h_d)
        for i in range(num_count):
            data = x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i]
            if ((x_d[i] + w_d[i]) - x_d[i]) <= (m_w * 3) and ((y_d[i] + h_d[i]) - y_d[i]) <= (m_h * 1.4):
                if len(array) == 0:
                    array = np.array([data])
                else:
                    sub = np.array(data)
                    array = np.vstack((sub, array))
    else:
        for i in range(num_count):
            data = x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i]
            if len(array) == 0:
                array = np.array([data])
            else:
                sub = np.array(data)
                array = np.vstack((sub, array))
    return array


def ocr_findcontour(image, array=np.array([])):
    gray_each = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret2, thresh_each = cv2.threshold(gray_each, 127, 255, cv2.THRESH_BINARY_INV)
    im2, ctrs2, hier2 = cv2.findContours(thresh_each.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    num_count = 0
    x_d = []
    y_d = []
    w_d = []
    h_d = []
    for j, c in enumerate(ctrs2):
        x, y, w, h = cv2.boundingRect(c)
        x_d.append(x)
        y_d.append(y)
        w_d.append(w)
        h_d.append(h)
        num_count += 1
    if len(w_d) >= 3 and len(h_d) >= 3:
        m_w, m_h = repeatly(w_d), repeatly(h_d)
        for i in range(num_count):
            data = x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i]
            if ((x_d[i] + w_d[i]) - x_d[i]) <= (m_w * 5) and ((y_d[i] + h_d[i]) - y_d[i]) <= (m_h * 2):
                if len(array) == 0:
                    array = np.array([data])
                else:
                    sub = np.array(data)
                    array = np.vstack((sub, array))
    else:
        for i in range(num_count):
            data = x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i]
            if len(array) == 0:
                array = np.array([data])
            else:
                sub = np.array(data)
                array = np.vstack((sub, array))
    return array


def ocr_mser(image, array=np.array([])):
    Rectangle = namedtuple('Rectangle', 'xmin ymin xmax ymax')
    mser = cv2.MSER_create()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    regions = mser.detectRegions(gray)
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions[0]]
    num_count = 0
    x_d = []
    y_d = []
    w_d = []
    h_d = []
    for i, c in enumerate(hulls):
        x, y, w, h = cv2.boundingRect(c)
        x_d.append(x)
        y_d.append(y)
        w_d.append(w)
        h_d.append(h)
        num_count += 1
    if len(w_d) >= 3 and len(h_d) >= 3:
        m_w, m_h = repeatly(w_d), repeatly(h_d)
        mean_w, mean_h = mean_mser(w_d), mean_mser(h_d)
        for i in range(num_count):
            if len(hulls) > 1:
                if i > 1:
                    a = Rectangle(x_d[i - 1], y_d[i - 1], x_d[i - 1] + w_d[i - 1], y_d[i - 1] + h_d[i - 1])
                    b = Rectangle(x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i])
                    if area_mser(a, b) is not None:
                        x, y, xm, ym = area_mser(a, b)
                        if (xm - x) <= (mean_w * 0.2) and (ym - y) <= (m_h * 2):
                            data = x, y, xm, ym
                            if len(array) == 0:
                                array = np.array([data])
                            else:
                                sub = np.array(data)
                                array = np.vstack((sub, array))
            else:
                data = x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i]
                array = np.array([data])
    else:
        for i in range(num_count):
            if len(hulls) > 1:
                if i > 1:
                    a = Rectangle(x_d[i - 1], y_d[i - 1], x_d[i - 1] + w_d[i - 1], y_d[i - 1] + h_d[i - 1])
                    b = Rectangle(x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i])
                    if area_mser(a, b) is not None:
                        x, y, xm, ym = area_mser(a, b)
                        data = x, y, xm, ym
                        if len(array) == 0:
                            array = np.array([data])
                        else:
                            sub = np.array(data)
                            array = np.vstack((sub, array))
            else:
                data = x_d[i], y_d[i], x_d[i] + w_d[i], y_d[i] + h_d[i]
                array = np.array([data])

    return array


def non_max_suppression_slow(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list, add the index
        # value to the list of picked indexes, then initialize
        # the suppression list (i.e. indexes that will be deleted)
        # using the last index
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        suppress = [last]
        # loop over all indexes in the indexes list
        for pos in range(0, last):
            # grab the current index
            j = idxs[pos]

            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = max(x1[i], x1[j])
            yy1 = max(y1[i], y1[j])
            xx2 = min(x2[i], x2[j])
            yy2 = min(y2[i], y2[j])

            # compute the width and height of the bounding box
            w = max(0, xx2 - xx1 + 1)
            h = max(0, yy2 - yy1 + 1)

            overlap = float(w * h) / area[j]

            if overlap > overlapThresh:
                suppress.append(pos)
        idxs = np.delete(idxs, suppress)
    return boxes[pick]


def get_data_list_xmin(data):
    return data[0]


def get_data_list_ymin(data):
    return data[1]


def cha_segment(image, verbose=False):
    image, x_c, y_c = crop_frame_picture(image)
    image = cv2.fastNlMeansDenoisingColored(image, None, 10, 10, 7, 21)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)
    kernel = np.ones((7, 100), np.uint8)
    closing = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
    img_dilation = cv2.dilate(closing, kernel, iterations=2)
    im, ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])

    image_list = []
    cg_img_list = []
    data_space = []
    row_bnd_list = []
    top_level = []
    mid_level = []
    bot_level = []

    for i, ctr in enumerate(sorted_ctrs):
        x, y, w, h = cv2.boundingRect(ctr)
        data_space.append((x, y, w + x, h + y))
    new_data_space = sorted(data_space, key=get_data_list_ymin)
    for i in range(len(new_data_space)):
        if i < (len(new_data_space) - 1):
            if new_data_space[i][1] == new_data_space[i + 1][1]:
                if new_data_space[i][0] <= new_data_space[i + 1][0]:
                    x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                       new_data_space[i][3]
                    roi = image[y:ymax, x:xmax]
                    cg_img_list.append((y, (y + ymax) / 2))
                    image_list.append(roi)
                    row_bnd_list.append((x + x_c, y + y_c, xmax + x_c, ymax + y_c))
                else:
                    keep_data = new_data_space[i]
                    new_data_space[i] = new_data_space[i + 1]
                    new_data_space[i + 1] = keep_data
                    x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                       new_data_space[i][3]
                    roi = image[y:ymax, x:xmax]
                    cg_img_list.append((y, (y + ymax) / 2))
                    image_list.append(roi)
                    row_bnd_list.append((x + x_c, y + y_c, xmax + x_c, ymax + y_c))
            else:
                x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], new_data_space[i][
                    3]
                roi = image[y:ymax, x:xmax]
                cg_img_list.append((y, (y + ymax) / 2))
                image_list.append(roi)
                row_bnd_list.append((x + x_c, y + y_c, xmax + x_c, ymax + y_c))

        else:
            x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], new_data_space[i][3]
            roi = image[y:ymax, x:xmax]
            cg_img_list.append((y, (y + ymax) / 2))
            image_list.append(roi)
            row_bnd_list.append((x + x_c, y + y_c, xmax + x_c, ymax + y_c))

    lst_bnd = []
    for num, img in enumerate(image_list):
        bnd = tesseract_seg(img)
        bnd = ocr_findcontour(img, bnd)
        # bnd = ocr_findcontour(img)
        bnd = ocr_mser(img, bnd)
        # bnd = ocr_mser(img)
        img_bnd = [(img, bnd)]
        for (imagePath, boundingBoxes) in img_bnd:
            sub_bnd = []
            image_nms = imagePath
            orig = image.copy()
            pick = non_max_suppression_slow(boundingBoxes, 0.3)
            new_pick = np.array([])
            new_data_space = sorted(pick, key=get_data_list_xmin)
            for i in range(len(new_data_space)):
                if i < (len(new_data_space) - 2):
                    if new_data_space[i][2] - new_data_space[i + 1][0] < (
                            ((new_data_space[i][2] - new_data_space[i][0]) / 100) * 35) and new_data_space[i][2] - \
                            new_data_space[i + 1][0] > 1 and new_data_space[i + 1][2] != new_data_space[i][2]:
                        if new_data_space[i + 1][1] < new_data_space[i][1]:
                            if new_data_space[i + 1][1] < new_data_space[i + 2][1]:
                                keep_data = new_data_space[i + 1]
                                new_data_space[i + 1] = new_data_space[i + 2]
                                new_data_space[i + 2] = keep_data
                                x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                                   new_data_space[i][3]
                            else:
                                x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                                   new_data_space[i][3]
                        else:
                            x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                               new_data_space[i][3]
                    elif new_data_space[i][0] == new_data_space[i + 1][0]:
                        if new_data_space[i][1] >= new_data_space[i + 1][1]:
                            x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                               new_data_space[i][3]
                        else:
                            keep_data = new_data_space[i]
                            new_data_space[i] = new_data_space[i + 1]
                            new_data_space[i + 1] = keep_data
                            x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                               new_data_space[i][3]
                    else:
                        x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                           new_data_space[i][3]
                    if len(new_pick) == 0:
                        new_pick = np.array([(x, y, xmax, ymax)])
                    else:
                        sub = np.array((x, y, xmax, ymax))
                        new_pick = np.vstack((new_pick, sub))
                elif i == (len(new_data_space) - 2):
                    if new_data_space[i][2] - new_data_space[i + 1][0] < (
                            ((new_data_space[i][2] - new_data_space[i][0]) / 100) * 35) and new_data_space[i][2] - \
                            new_data_space[i + 1][0] > 1 and new_data_space[i + 1][2] != new_data_space[i][2]:
                        if new_data_space[i][1] >= new_data_space[i + 1][1]:
                            x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                               new_data_space[i][3]
                        else:
                            keep_data = new_data_space[i]
                            new_data_space[i] = new_data_space[i + 1]
                            new_data_space[i + 1] = keep_data
                            x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                               new_data_space[i][3]
                    else:
                        x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                           new_data_space[i][3]
                    if len(new_pick) == 0:
                        new_pick = np.array([(x, y, xmax, ymax)])
                    else:
                        sub = np.array((x, y, xmax, ymax))
                        new_pick = np.vstack((new_pick, sub))
                else:
                    x, y, xmax, ymax = new_data_space[i][0], new_data_space[i][1], new_data_space[i][2], \
                                       new_data_space[i][3]
                    if len(new_pick) == 0:
                        new_pick = np.array([(x, y, xmax, ymax)])
                    else:
                        sub = np.array((x, y, xmax, ymax))
                        new_pick = np.vstack((new_pick, sub))

            cg_y_start = cg_img_list[num][0]
            cg_y_level = cg_img_list[num][1] - cg_y_start
            mid_keep = {}
            top_keep = {}
            bot_keep = {}
            # Threshold for find ratio level of character
            # edit for change ratio 0 - 100 (percent)
            threshold = 50
            #######################
            if len(new_pick) >= 3:
                for sub_num, (startX, startY, endX, endY) in enumerate(new_pick):
                    data = startX, startY, endX, endY
                    level = (startY + endY) / 2
                    if cg_y_level + ((cg_y_level / 100) * threshold) >= level >= cg_y_level - \
                            ((cg_y_level / 200) * threshold):
                        mid_keep[sub_num] = (startX, startY, endX, endY)
                    elif level > cg_y_level + ((cg_y_level / 100) * threshold):
                        bot_keep[sub_num] = (startX, startY, endX, endY)
                    else:
                        top_keep[sub_num] = (startX, startY, endX, endY)
                    sub_bnd.append(data)
                    if verbose:
                        cv2.rectangle(image_nms, (startX, startY), (endX, endY), (0, 255, 0), 1)
                mid_level.append(mid_keep.copy())
                top_level.append(top_keep.copy())
                bot_level.append(bot_keep.copy())
                lst_bnd.append(sub_bnd)
            else:
                mid_level.append(mid_keep.copy())
                top_level.append(top_keep.copy())
                bot_level.append(bot_keep.copy())
                lst_bnd.append(sub_bnd)
    return lst_bnd, image_list, row_bnd_list, mid_level, top_level, bot_level


class NIA:
    def __init__(self, input_dict=None, input_list=None):
        self.number = input_dict['content']
        self.row = input_dict['row']
        self.pos = input_dict['position']
        self.distance = None
        self.score = 0
        self.order = input_dict['order']
        self.confidence = 0
        self.len_number = len(self.number)
        self.max_order = len(input_list)
        self.max_row = input_dict['max_row']

    def add_weight(self, slash_w=20, row_w=10, pos_w=10, order_w=10, len_w=20):
        self.slash_w = slash_w
        self.row_w = row_w
        self.pos_w = pos_w
        self.order_w = order_w
        self.len_w = len_w
        self.max_w = self.slash_w + self.row_w + self.pos_w + self.order_w + self.len_w

    def compute(self):
        if '/' in self.number:
            self.score += self.slash_w
        self.score += self.row_w - (self.row * 1)
        self.score += self.order_w - (self.order * 1)
        self.score += self.len_w - (abs(self.len_number - 6.5) * 2)

    def return_score(self):
        return (self.score / self.max_w) * 10


def all_number_set(word_list, row_list, index_list, bnd_list, row_bnd_list):
    dic = {}
    num_list = []
    word_all = ''
    old_index = 0
    bnd = []
    check = False
    max_row = len(row_list)
    for i, row in enumerate(row_list):
        data_count = 0
        order = 1
        for word in word_list[i]:
            for e_w in word:
                data_count += 1
        for word in word_list[i]:
            count = 0
            for e_w in word:
                if not check:
                    word_all += e_w
                    bnd.append(bnd_list[row][index_list[i][count]])
                    old_index = bnd_list[row][index_list[i][count]]
                    check = True
                elif old_index == bnd_list[row][index_list[i][count] - 1]:
                    word_all += e_w
                    old_index = bnd_list[row][index_list[i][count]]
                    bnd.append(bnd_list[row][index_list[i][count]])
                elif old_index != bnd_list[row][index_list[i][count] - 1]:
                    xmin, ymin, xmax, ymax = float("inf"), float("inf"), 0, 0
                    for data in bnd:
                        if xmin >= data[0]:
                            xmin = data[0]
                        if xmax <= data[2]:
                            xmax = data[2]
                        if ymin >= data[1]:
                            ymin = data[1]
                        if ymax <= data[3]:
                            ymax = data[3]
                    cg_x = ((xmax + xmin) / 2) + row_bnd_list[row][0]
                    cg_y = ((ymax + ymin) / 2) + row_bnd_list[row][1]
                    dic['content'] = word_all
                    dic['row'] = row + 1
                    dic['position'] = (cg_x, cg_y)
                    dic['order'] = order
                    dic['max_row'] = max_row
                    num_list.append(dic.copy())
                    order += 1
                    word_all = ''
                    bnd = []
                    check = False
                    old_index = bnd_list[row][index_list[i][count]]
                    word_all += e_w
                    bnd.append(bnd_list[row][index_list[i][count]])
                if count == data_count - 1:
                    xmin, ymin, xmax, ymax = float("inf"), float("inf"), 0, 0
                    for data in bnd:
                        if xmin >= data[0]:
                            xmin = data[0]
                        if xmax <= data[2]:
                            xmax = data[2]
                        if ymin >= data[1]:
                            ymin = data[1]
                        if ymax <= data[3]:
                            ymax = data[3]
                    cg_x = ((xmax + xmin) / 2) + row_bnd_list[row][0]
                    cg_y = ((ymax + ymin) / 2) + row_bnd_list[row][1]
                    dic['content'] = word_all
                    dic['row'] = row + 1
                    dic['position'] = (cg_x, cg_y)
                    dic['order'] = order
                    dic['max_row'] = max_row
                    num_list.append(dic.copy())
                    word_all = ''
                    bnd = []
                    check = False
                count += 1
    return num_list


def mean_length_x(dict_data):
    num = 0
    for i in dict_data:
        num = num + abs(dict_data[i][0] - dict_data[i][2])
    num = num / len(dict_data)
    return num


def clear_list_data(list_data):
    lst_return = []
    use_key = []
    for data_1 in list_data:
        new_list = []
        check_copy = False
        if data_1 not in use_key:
            for data_2 in list_data:
                if data_1 != data_2:
                    if list(set(data_1) & set(data_2)) != [] or list(set(new_list) & set(data_2)) != []:
                        new_list += list(set(data_1) | set(data_2))
                        use_key.append(data_2)
                        check_copy = True
                    else:
                        pass
            if check_copy == False:
                data_1 = list(set(data_1))
                lst_return.append(sorted(data_1))
            else:
                new_list = list(set(sorted(new_list)))
                lst_return.append(sorted(new_list))
    return lst_return


def grouping_word(top, mid, bot, threshold=1.7):
    size_line = len(mid)
    group_key_lst = []
    for line in range(size_line):
        group_line = []
        all_dict = {}
        lst = [top[line], mid[line], bot[line]]
        for i in lst:
            for j in i:
                all_dict[j] = (i[j])
        size_word = len(all_dict)
        try:
            mean_x = mean_length_x(all_dict) * threshold
        except:
            mean_x = 0

        for word in range(size_word):
            group_word = []
            count = 1
            cg_x_main = (all_dict[word][0] + all_dict[word][2]) / 2
            for data in all_dict:
                if count == 1:
                    group_word.append(word)
                    count = 2
                if abs(cg_x_main - ((all_dict[data][0] + all_dict[data][2]) / 2)) < mean_x:
                    group_word.append(data)
                else:
                    pass
            group_line.append(group_word)
        clear_group_line = clear_list_data(group_line)
        group_key_lst.append(clear_group_line)
    return group_key_lst


class Number_in_Address:
    def __init__(self, input_dict=None, input_list=None):
        self.number = input_dict['content']
        self.row = input_dict['row']
        self.pos = input_dict['position']
        self.distance = None
        self.score = 0
        self.order = input_dict['order']
        self.confidence = 0
        self.len_number = len(self.number)
        self.max_order = len(input_list)
        self.max_row = input_dict['max_row']

    def add_weight(self, slash_w=20, row_w=10, pos_w=10, order_w=10, len_w=20):
        self.slash_w = slash_w
        self.row_w = row_w
        self.pos_w = pos_w
        self.order_w = order_w
        self.len_w = len_w
        self.max_w = self.slash_w + self.row_w + self.pos_w + self.order_w + self.len_w

    def compute(self):
        if '/' in self.number:
            self.score += self.slash_w
        self.score += self.row_w - (self.row * 1)
        self.score += self.order_w - (self.order * 1)
        self.score += self.len_w - (abs(self.len_number - 6.5) * 2)

    def return_score(self):
        return (self.score / self.max_w) * 10


def get_Address(input, debug=0):
    Numbers_in_Address = []
    for i in input:
        Numbers_in_Address.append(Number_in_Address(input_list=input, input_dict=i))

        if debug:
            print(i)

    for content in Numbers_in_Address:
        content.add_weight()
        content.compute()
        if debug == 1:
            print("Number is {} and Score is {}".format(content.number, content.return_score()))

    return max(Numbers_in_Address, key=lambda x: x.score).number
