import pygame
import os
import pandas as pd
import time
import cv2
import numpy as np
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import random
import serial
import struct
import threading
import utilities as utils
from Model import Model

model = Model()

robot_working = True
take_data_timing = True
trigger = 0
add_file = 0
list_new = []
array = []
list_string = []
string_ready = False
data = []
out_from_GUI = 0
via_point = []
warehouse = {
        (1, 1): "Null",
        (1, 2): "Null",
        (1, 3): "Null",
        (2, 1): "Null",
        (2, 2): "Null",
        (2, 3): "Null",
        (3, 1): "Null",
        (3, 2): "Null",
        (3, 3): "Null",
        (3, 4): "Null",
        (4, 1): "Null",
        (4, 2): "Null",
        (4, 3): "Null",
        (4, 4): "Null"
    }


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        global trigger
        global add_file
        global list_new
        global data
        global out_from_GUI
        if trigger == 1:
            if event.src_path == "communicating/configuration_motor.csv":
                # print("Reading data from MATLAB.")
                file = open("communicating/configuration_motor.csv", "r")
                for line in file:
                    data = line.split(",")
                for i in data:
                    i = float(i)
                    list_new.append(i)
                list_new[3] = list_new[3] + 90
                list_new[4] = list_new[4] + 90
                if out_from_GUI == 0:
                    list_new[2] = -list_new[2]
                    list_new[4] = -list_new[4]
                list_new[2] = list_new[2] + 64
                list_new[5] = list_new[5] + 90
                print('check data =', list_new)
                add_file = 1
            trigger = 0
        else:
            trigger += 1


def gen_box(screen,x,y,x_plus,y_plus,typing,mouse,font_size = 20):
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, x_plus, y_plus))
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x, y, x_plus, y_plus), 5)
    text_input(screen, x, y, x_plus, y_plus, '{}'.format(typing),font_size)
    if x < mouse[0] < x + x_plus and y < mouse[1] < y + y_plus:
        pygame.draw.rect(screen, (0, 200, 0), pygame.Rect(x, y, x_plus, y_plus), 5)


def blit_alpha(target, source, location, opacity):
    x = location[0]
    y = location[1]
    temp = pygame.Surface((source.get_width(), source.get_height())).convert()
    temp.blit(target, (-x, -y))
    temp.blit(source, (0, 0))
    temp.set_alpha(opacity)
    target.blit(temp, location)


def command_movepage(screen, page, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data, joint_limit, cartesian_limit, warehouse, data_storage):
    if page == 0:
        page_zero(screen, mouse, x_setting, y_setting)
    elif page == 1:
        page_one(mouse, screen, warehouse, data_storage)
    elif page == 2:
        page_two(screen, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data, joint_limit,
                 cartesian_limit)


def animation(screen, time_current, img1, spin_01, spin_02, change_page, page, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data, joint_limit, cartesian_limit, old_page, random_result, warehouse,data_storage):
    # event_smoke = 0
    # appear_smoke = 0
    if time_current == 0:
        time_current = time.time()
    elapsed_time = time.time() - time_current
    time_left = 1.0 - elapsed_time

    if time_left >= -1 and time_left <= 0:  # trigger stop rushmore
        percent_movement = 0
        # event_smoke = 1
        # appear_smoke = 255 / (time_left+1)
    else:
        percent_movement = time_left * (1 / 1.0)

    percent_movement_2 = time_left * 2
    # percent_movement_smoke_1 = (1-time_left) * 10
    # percent_movement_smoke_2 = (1-time_left) * 1
    # percent_movement_smoke_3 = (1-time_left) * 0.25

    if time_left <= -2.0:  # end event
        time_current = time.time()
        change_page = 0

    if time_left <= 0:  # change background
        command_movepage(screen, page, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data,
                         joint_limit, cartesian_limit, warehouse, data_storage)
    else:
        command_movepage(screen, old_page, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data,
                         joint_limit, cartesian_limit, warehouse, data_storage)

    if percent_movement >= 0:  # rushmore event
        blit_alpha(screen, img1, (0, -740 * percent_movement), 255)
    else:
        event_smoke = 0
        blit_alpha(screen, img1, (0, 740 * (percent_movement + 1)), 255)

    if random_result == 1:  # O-f event
        blit_alpha(screen, spin_01, (300, -740 * percent_movement_2), 255)
    elif random_result == 2:
        blit_alpha(screen, spin_02, (880, -740 * percent_movement_2), 255)
    elif random_result == 5:
        blit_alpha(screen, spin_01, (300, -740 * percent_movement_2), 255)
        blit_alpha(screen, spin_02, (880, -740 * percent_movement_2), 255)

    # if event_smoke == 1:  # smoke rushmore event
    #     if time_left >= -0.5:
    #         blit_alpha(screen, smoke_l1, (0, 200 * percent_movement_smoke_1), appear_smoke)
    #         blit_alpha(screen, smoke_l2, (0, 250 * percent_movement_smoke_1), appear_smoke)
    #         blit_alpha(screen, smoke_l3, (0, 300 * percent_movement_smoke_1), appear_smoke)
    #     elif time_left >= -0.8:
    #         blit_alpha(screen, smoke_l1, (0, 200 * percent_movement_smoke_2), appear_smoke)
    #         blit_alpha(screen, smoke_l2, (0, 250 * percent_movement_smoke_2), appear_smoke)
    #         blit_alpha(screen, smoke_l3, (0, 300 * percent_movement_smoke_2), appear_smoke)
    #     else:
    #         blit_alpha(screen, smoke_l1, (0, 200 * percent_movement_smoke_3), appear_smoke)
    #         blit_alpha(screen, smoke_l2, (0, 250 * percent_movement_smoke_3), appear_smoke)
    #         blit_alpha(screen, smoke_l3, (0, 300 * percent_movement_smoke_3), appear_smoke)

    return time_current, change_page


def text_objects(text, font, color=(0, 0, 0)):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


def del_area(screen, x, y, group=1):
    if group == 1:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, int(150), int(320 / 3)))
    elif group == 2:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, int(150), int(320 / 4)))
    else:
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, int(200), int(100)))


def textHollow(font, message, fontcolor):
    notcolor = [c^0xFF for c in fontcolor]
    base = font.render(message, 0, fontcolor, notcolor)
    size = base.get_width() + 2, base.get_height() + 2
    img = pygame.Surface(size, 16)
    img.fill(notcolor)
    base.set_colorkey(0)
    img.blit(base, (0, 0))
    img.blit(base, (2, 0))
    img.blit(base, (0, 2))
    img.blit(base, (2, 2))
    base.set_colorkey(0)
    base.set_palette_at(1, notcolor)
    img.blit(base, (1, 1))
    img.set_colorkey(notcolor)
    return img


def textOutline(font, message, fontcolor, outlinecolor):
    base = font.render(message, 0, fontcolor)
    outline = textHollow(font, message, outlinecolor)
    img = pygame.Surface(outline.get_size(), 16)
    img.blit(base, (1, 1))
    img.blit(outline, (0, 0))
    img.set_colorkey(0)
    return img


def text_input(screen, x, y, size_x, size_y, text, font_size=20, mode=0):
    font = "Thaimono.ttf"
    if mode == 2:
        font = "test_sans.ttf"
        font_size = 25
    smallText = pygame.font.Font("{}".format(font), font_size)
    white = 255, 255, 255
    grey = 100, 100, 100
    if text == 'Block' or mode == 1 and text != 'Null':
        textSurf, textRect = text_objects("{}".format(text), smallText, color=(255, 0, 0))
    elif text == 'Null':
        textSurf, textRect = text_objects("{}".format(text), smallText, color=(0, 0, 255))
    elif mode == 2:
        textSurf = textOutline(smallText, '{}'.format(text), grey, white)
        _, textRect = text_objects("{}".format(text), smallText, color=(0, 0, 0))
    else:
        textSurf, textRect = text_objects("{}".format(text), smallText, color=(0, 0, 0))
    textRect.center = ((x + (size_x / 2)), (y + (size_y / 2)))
    blit_alpha(screen, textSurf, (textRect[0], textRect[1]), 255)


def page_zero(screen, mouse, x_setting, y_setting):
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x_setting, y_setting, x_setting + 110, 100))
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x_setting, y_setting, x_setting + 110, 100), 5)  # 480+160 380+80
    text_input(screen, x_setting, y_setting, x_setting + 110, 100, 'Manual Input')
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x_setting, y_setting + 150, x_setting + 110, 100))
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x_setting, y_setting + 150, x_setting + 110, 100), 5)
    text_input(screen, x_setting, y_setting + 150, x_setting + 110, 100, 'Manual Control Articulated')
    if mouse[0] in range(x_setting, 2 * x_setting + 110) and mouse[1] in range(y_setting, y_setting + 100):
        pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(x_setting, y_setting, x_setting + 110, 100), 5)
    elif mouse[0] in range(x_setting, 2 * x_setting + 110) and mouse[1] in range(y_setting + 150, y_setting + 250):
        pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(x_setting, y_setting + 150, x_setting + 110, 100), 5)


def page_one(mouse, screen, warehouse, data_storage):
    for x in range(320, 640, 160):
        for y in range(380, 380 + 3 * (int(320 / 3)), int(320 / 3)):
            rows = int((x - 320) / 160) + 1
            column = int((y - 380) / int(320 / 3)) + 1
            if y > 100:
                pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, int(160), int(320 / 3) + 2))
                pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x, y, int(160), int(320 / 3) + 2), 5)
                if x < mouse[0] < x + 160 and y < mouse[1] < y + int(320 / 3):  # pointed
                    pygame.draw.rect(screen, (0, 200, 0), pygame.Rect(x, y, int(160), int(320 / 3) + 2), 5)
            else:
                pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, int(160), int(320 / 3)))
                pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x, y, int(160), int(320 / 3)), 5)
                if x < mouse[0] < x + 160 and y < mouse[1] < y + int(320 / 3):  # pointed
                    pygame.draw.rect(screen, (0, 200, 0), pygame.Rect(x, y, int(160), int(320 / 3)), 5)
            for dict_hold_1, dict_hold_2 in warehouse.items():
                if dict_hold_1 == (rows, column):
                    dict_hold_2 = dict_hold_2.split(' ')
                    if len(dict_hold_2) > 1:
                        number_loop = 0
                        for text_inside in dict_hold_2:
                            text_input(screen, x, y+int((320 / 3)/len(dict_hold_2))*number_loop, 160, int((320 / 3)/len(dict_hold_2)), text_inside, mode=1, font_size=12)
                            number_loop += 1
                    else:
                        text_input(screen, x, y, 160, int(320 / 3), dict_hold_2, mode=1, font_size=12)

    for x in range(640, 960, 160):
        for y in range(380, 380 + 4 * (int(320 / 4)), int(320 / 4)):
            rows = int((x - 640) / 160) + 3
            column = int((y - 380) / int(320 / 4)) + 1
            pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(x, y, int(160), int(320 / 4)))
            pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(x, y, int(160), int(320 / 4)), 5)
            for dict_hold_1, dict_hold_2 in warehouse.items():
                if dict_hold_1 == (rows, column):
                    dict_hold_2 = dict_hold_2.split(' ')
                    if len(dict_hold_2) > 1:
                        number_loop = 0
                        for text_inside in dict_hold_2:
                            text_input(screen, x, y + int((320 / 4) / len(dict_hold_2)) * number_loop, 160,int((320 / 4) / len(dict_hold_2)), text_inside, mode=1, font_size=12)
                            number_loop += 1
                    else:
                        text_input(screen, x, y, 160, int(320 / 4), dict_hold_2, mode=1, font_size=12)

            if x < mouse[0] < x + 160 and y < mouse[1] < y + int(320 / 4):
                pygame.draw.rect(screen, (0, 200, 0), pygame.Rect(x, y, int(160), int(320 / 4)), 5)

    gen_box(screen, 1030, 425, 200, 100, 'bin', mouse)

    gen_box(screen, 1030, 250, 200, 100, 'Search', mouse)

    gen_box(screen, 1030, 600, 200, 100, data_storage, mouse)

    gen_box(screen, 50, 50, 200, 100, "Home", mouse)


def page_two(screen, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data, joint_limit,cartesian_limit):
    width = 1280
    for i in range(y_setting - 5, y_setting + 185, int(75 / 2)):
        index = int((i - y_setting + 5) / int(75 / 2))
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(150, i, x_setting, int(75 / 2)))
        pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(150, i, x_setting, int(75 / 2)), 5)
        text_input(screen, 80, i - 5, 50, 50, '{}'.format(Cartesian[index]),mode=2)
        text_input(screen, 150, i, x_setting, int(75 / 2), '{}'.format(cartesian_data[index]))
        if 150 < mouse[0] < 150 + x_setting and i < mouse[1] < i + int(75 / 2):
            pygame.draw.rect(screen, (0, 200, 0), pygame.Rect(150, i, x_setting, int(75 / 2)), 5)
            # pygame.draw.rect(screen, (255, 255, 255),pygame.Rect(mouse[0], mouse[1], int(150), int(-50)))  # detail cartesian limit
            # pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(mouse[0], mouse[1], int(150), int(-50)),5)  # detail cartesian limit
            # text_input(mouse[0], mouse[1], 150, -50,('{0},{1}').format(cartesian_limit[((index+1)*2)-2],cartesian_limit[((index+1)*2)-1]))

    gen_box(screen, 250, y_setting + 230, 100, 50, 'submit', mouse)

    for i in range(y_setting - 5, y_setting + 185, int(75 / 2)):
        index = int((i - y_setting + 5) / int(75 / 2))
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(width - x_setting - 100, i, x_setting, int(75 / 2)))
        pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(width - x_setting - 100, i, x_setting, int(75 / 2)), 5)
        text_input(screen, width - x_setting - 160, i - 5, 50, 50, '{}'.format(joint[index]),mode=2)
        text_input(screen, width - x_setting - 100, i, x_setting, int(75 / 2), '{}'.format(joint_data[index]))
        if width - x_setting - 100 < mouse[0] < width - x_setting - 100 + x_setting and i < mouse[1] < i + int(75 / 2):
            pygame.draw.rect(screen, (0, 200, 0), pygame.Rect(width - x_setting - 100, i, x_setting, int(75 / 2)), 5)
            pygame.draw.rect(screen, (255, 255, 255),pygame.Rect(mouse[0], mouse[1], int(100), int(-50)))  # detail joint limit
            pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(mouse[0], mouse[1], int(100), int(-50)),5)  # detail joint limit
            text_input(screen, mouse[0], mouse[1], 100, -50,('{0},{1}').format(joint_limit[((index + 1) * 2) - 2], joint_limit[((index + 1) * 2) - 1]))

    gen_box(screen, width - x_setting + 80, y_setting + 230, 100, 50, 'submit', mouse)

    gen_box(screen, width - x_setting + 130, 250, 100, 100, 'Zero', mouse)

    gen_box(screen, width - x_setting + 80, 300, 50, 50, 'v-off', mouse, font_size=15)

    gen_box(screen, width - x_setting + 30, 300, 50, 50, 'v-on', mouse, font_size=15)

    gen_box(screen, width - x_setting + 80, 250, 50, 50, 'f2', mouse)

    gen_box(screen, width - x_setting + 30, 250, 50, 50, 'f1', mouse)

    gen_box(screen, 50, 50, 200, 100, 'Home', mouse)


def page_one_controller(active, page, x_pos_click, y_pos_click, hold_data, data_storage, data_inside, text, warehouse):
    old_page = page
    change_page = 0
    if x_pos_click in range(320, 640) and y_pos_click in range(380, 380 + 3 * (int(320 / 3))):
        rows = int((x_pos_click - 320) / 160) + 1
        column = int((y_pos_click - 380) / int(320 / 3)) + 1
        if type(warehouse[(rows, column)]) == str:
            if warehouse[(rows, column)] == 'Null':
                if type(hold_data) == str or type(hold_data) == int or type(hold_data) == list:
                    if hold_data:
                        warehouse[(rows, column)] = hold_data
                        hold_data = []
                else:
                    warehouse[(rows, column)] = hold_data
                    hold_data = []
            else:
                if type(hold_data) == str or type(hold_data) == int or type(hold_data) == list:
                    if not hold_data:
                        data_inside = warehouse.get((rows, column))
                        hold_data = data_inside
                        data_inside = []
                        warehouse[(rows, column)] = 'Null'
                else:
                    data_inside = warehouse.get((rows, column))
                    hold_data = data_inside
                    data_inside = []
                    warehouse[(rows, column)] = 'Null'
        else:
            if type(hold_data) == str or type(hold_data) == int or type(hold_data) == list:
                if not hold_data:
                    data_inside = warehouse.get((rows, column))
                    hold_data = data_inside
                    data_inside = []
                    warehouse[(rows, column)] = 'Null'
            else:
                data_inside = warehouse.get((rows, column))
                hold_data = data_inside
                data_inside = []
                warehouse[(rows, column)] = 'Null'

    elif x_pos_click in range(640, 960) and y_pos_click in range(380, 380 + 4 * (int(320 / 3))):
        rows = int((x_pos_click - 640) / 160) + 3
        column = int((y_pos_click - 380) / int(320 / 4)) + 1
        if type(warehouse[(rows, column)]) == str:
            if warehouse[(rows, column)] == 'Null':
                if type(hold_data) == str or type(hold_data) == int or type(hold_data) == list:
                    if hold_data:
                        warehouse[(rows, column)] = hold_data
                        hold_data = []
                else:
                    warehouse[(rows, column)] = hold_data
                    hold_data = []
            else:
                if type(hold_data) == str or type(hold_data) == int or type(hold_data) == list:
                    if not hold_data:
                        data_inside = warehouse.get((rows, column))
                        hold_data = data_inside
                        data_inside = []
                        warehouse[(rows, column)] = 'Null'
                else:
                    data_inside = warehouse.get((rows, column))
                    hold_data = data_inside
                    data_inside = []
                    warehouse[(rows, column)] = 'Null'
        else:
            if type(hold_data) == str or type(hold_data) == int or type(hold_data) == list:
                if not hold_data:
                    data_inside = warehouse.get((rows, column))
                    hold_data = data_inside
                    data_inside = []
                    warehouse[(rows, column)] = 'Null'
            else:
                data_inside = warehouse.get((rows, column))
                hold_data = data_inside
                data_inside = []
                warehouse[(rows, column)] = 'Null'

    elif x_pos_click in range(1030, 1230) and y_pos_click in range(425, 425 + 100):
        hold_data = []

    elif x_pos_click in range(1030, 1230) and y_pos_click in range(600, 700):
        if not data_storage:
            if hold_data:
                active = False
                data_storage = hold_data
                hold_data = []
            else:  # data = [],hold = []
                active = not active
                text = ''
        else:
            active = False
            if not hold_data:
                hold_data = data_storage
                data_storage = []
                text = ''
    elif x_pos_click in range(50, 300) and y_pos_click in range(50, 150):
        # old_page = page
        change_page = 1
        page = 0
    return active, page, hold_data, data_storage, text, data_inside, change_page, old_page


def page_two_controller(active_page_2, page, x_setting, y_setting, x_pos_click, y_pos_click, cartesian_data, joint_data, text, Articulated_df, joint_limit, cartesian_limit, joint_df, comd_pneumatic):
    global out_from_GUI

    old_page = page
    change_page = 0
    width = 1280
    for i in range(y_setting - 5, y_setting + 185, int(75 / 2)):  # Cartesian control
        if x_pos_click in range(150, x_setting + 50) and y_pos_click in range(i, i + int(75 / 2)):
            index = int((i - y_setting + 5) / int(75 / 2))
            text = ''
            active_page_2 = [False, False, False, False, False, False, False, False, False, False, False, False]
            active_page_2[index] = not active_page_2[index]
    for l in range(y_setting - 5, y_setting + 185, int(75 / 2)):
        if x_pos_click in range(width - x_setting - 100, width - 100) and y_pos_click in range(l, l + int(75 / 2)):
            index = int((l - y_setting + 5) / int(75 / 2))
            text = ''
            active_page_2 = [False, False, False, False, False, False, False, False, False, False, False, False]
            active_page_2[index + 6] = not active_page_2[index + 6]
    if x_pos_click in range(50, 300) and y_pos_click in range(50, 150):
        change_page = 1
        page = 0
    elif x_pos_click in range(width - x_setting + 30, width - x_setting + 30 + 50) and y_pos_click in range(250,250 + 50):
        comd_pneumatic = 'open'
    elif x_pos_click in range(width - x_setting + 80, width - x_setting + 80 + 50) and y_pos_click in range(250,250 + 50):
        comd_pneumatic = 'close'
    elif x_pos_click in range(width - x_setting + 30, width - x_setting + 30 + 50) and y_pos_click in range(300,300 + 50):
        comd_pneumatic = 'via_on'
    elif x_pos_click in range(width - x_setting + 80, width - x_setting + 80 + 50) and y_pos_click in range(300,300 + 50):
        comd_pneumatic = 'via_off'
    elif x_pos_click in range(width - x_setting + 130, width - x_setting + 130 + 100) and y_pos_click in range(250,250 + 100):
        comd_pneumatic = 'zero'
    elif x_pos_click in range(250, 350) and y_pos_click in range(y_setting + 225, y_setting + 275):
        if cartesian_data[0] == '' or cartesian_data[1] == '' or cartesian_data[2] == '':
            print('cannot be empty value!')
        else:
            Articulated_df["X"] = cartesian_data[0]
            Articulated_df["Y"] = cartesian_data[1]
            Articulated_df["Z"] = cartesian_data[2]
            Articulated_df["Roll"] = cartesian_data[3]
            Articulated_df["Pitch"] = cartesian_data[4]
            Articulated_df["Yaw"] = cartesian_data[5]
            Articulated_df["q1"] = 0
            Articulated_df["q2"] = 0
            Articulated_df["q3"] = 0
            Articulated_df["q4"] = 0
            Articulated_df["q5"] = 0
            Articulated_df["q6"] = 0
            Articulated_df["type"] = 0
            out_from_GUI = 0
            Articulated_df.to_csv('communicating/configuration.csv',index=False, header=False)
            print('Command send')
    elif x_pos_click in range(width - x_setting + 80, width - x_setting + 180) and y_pos_click in range(y_setting + 230,
                                                                                                        y_setting + 280):
        count = 1
        count_point = 5
        for joint_move in joint_data:
            if joint_move == '':
                print('joint {} cannot be empty value!'.format(count))
            elif float(joint_limit[(count * 2) - 2]) > float(joint_move) or float(joint_move) > float(
                    joint_limit[(count * 2) - 1]):
                print('joint {} not in joint limit!'.format(count))
            elif count_point == 0:
                # Articulated_df["X"] = 0
                # Articulated_df["Y"] = 0
                # Articulated_df["Z"] = 0
                # Articulated_df["Roll"] = 0
                # Articulated_df["Pitch"] = 0
                # Articulated_df["Yaw"] = 0
                joint_df["q1"] = joint_data[0]
                joint_df["q2"] = joint_data[1]
                joint_df["q3"] = joint_data[2]
                joint_df["q4"] = joint_data[3]
                joint_df["q5"] = joint_data[4]
                joint_df["q6"] = joint_data[5]
                # Articulated_df["type"] = 1
                out_from_GUI = 1
                joint_df.to_csv('communicating/configuration_motor.csv',index=False, header=False)
                print('Command send')
            else:
                count_point -= 1
            count += 1
    return active_page_2, text, cartesian_data, joint_data, page, change_page, old_page, joint_df, comd_pneumatic


def to_warehouse(dataframe):  # for auto add data to warehouse
    global warehouse

    warehouse_free_list = []
    list_index = [(1, 1),(1, 2),(1, 3),(2, 1),(2, 2),(2, 3),(3, 1), (3, 2), (3, 3), (3, 4), (4, 1), (4, 2), (4, 3), (4, 4)]
    for x in warehouse:
        if warehouse[x] != 'Null' or x[0] <= 2:
            warehouse_free_list.append(x[1] + (x[0] * 4) + 100)
        else:
            warehouse_free_list.append(x[1] + (x[0] * 4))
    if min(warehouse_free_list) < 100:
        for y in range(len(warehouse_free_list)):
            if warehouse_free_list[y] == min(warehouse_free_list):
                warehouse[list_index[y]] = dataframe
    else:
        print('No free space left.')
    return warehouse_free_list


def int2byte(num):
    data = []
    hex_num = hex(num)
    c = len(hex_num) - 2
    get_hex = hex_num.split('x')
    byte_1 = ""
    byte_2 = ""
    if c == 4:
        byte_1 = "0x" + get_hex[1][0:2]
        byte_2 = "0x" + get_hex[1][2:]
    if c == 3:
        byte_1 = "0x" + get_hex[1][0:1]
        byte_2 = "0x" + get_hex[1][1:]
    if c == 2:
        byte_1 = "0x00"
        byte_2 = "0x" + get_hex[1]
    if c == 1:
        byte_1 = "0x00"
        byte_2 = "0x0" + get_hex[1]
    if c > 4 or c < 1:
        print("Data is wrong")
    data.append(byte_1)
    data.append(byte_2)
    return data


def inv_hex(_num):
    norm_hex = hex(_num)
    on_hex = norm_hex.split("x")
    invhex = "0x"
    if len(on_hex[1]) == 1:
        invhex = '0xf'
    for i in on_hex[1][-2:]:
        i_hex = hex(15 - int(i, base=16))
        cut_hex = i_hex.split("x")
        invhex += cut_hex[1]
    return int(invhex, base=16)


def senddata(command, data_1=None, data_2=None):
    global ser
    send_data = [0xff, 0xff, command]
    x_hex = int2byte(data_1)
    send_data.append(int(x_hex[0], base=16))
    send_data.append(int(x_hex[1], base=16))
    y_hex = int2byte(data_2)
    send_data.append(int(y_hex[0], base=16))
    send_data.append(int(y_hex[1], base=16))
    sum = command + int(x_hex[0], base=16) + int(x_hex[1], base=16) + int(y_hex[0], base=16) + int(y_hex[1], base=16)
    check_sum = inv_hex(sum)
    send_data.append(check_sum)
    print(bytes([send_data[0], send_data[1], send_data[2], send_data[3], send_data[4], send_data[5], send_data[6],
                 send_data[7]]))
    ser.write(bytes([send_data[0], send_data[1], send_data[2], send_data[3], send_data[4], send_data[5], send_data[6],
                     send_data[7]]))


def float2byte(_num):
    data = bytearray(struct.pack("f", _num))
    return data


def protocol_sending(command, q, sq):
    global ser
    """
    command is list of command protocol
    q is list of degree
    sq is sequence of protocol
    """
    print('hello', q[sq])
    print(type(q[sq]))
    data = float2byte(q[sq])
    checksum = inv_hex(command[sq] + data[0] + data[1] + data[2] + data[3])
    ser.write(bytes([0xff]))
    time.sleep(0.02)
    ser.write(bytes([0xff]))
    time.sleep(0.02)
    ser.write(bytes([command[sq]]))
    time.sleep(0.02)
    ser.write(bytes([data[0]]))
    time.sleep(0.02)
    ser.write(bytes([data[1]]))
    time.sleep(0.02)
    ser.write(bytes([data[2]]))
    time.sleep(0.02)
    ser.write(bytes([data[3]]))
    time.sleep(0.02)
    ser.write(bytes([checksum]))
    time.sleep(0.02)


def receive(data, array):
    try:
        count = len(array)
        if data[0] == 0xff and count == 0:
            array.append(data[0])
        elif data[0] == 0xff and count == 1:
            array.append(data[0])
        elif count == 2:
            if data[0] == 0x06:
                array.append(data[0])
            else:
                array = []
        elif 3 <= count < 7:
            if data[0] == 0x00:
                array.append(data[0])
            else:
                array = []
                return False, array
        elif count == 7:
            chk_sum_re_inv = inv_hex(array[2] + array[3] + array[4] + array[5] + array[6])
            array.append(data[0])
            if chk_sum_re_inv == data[0]:
                # print("test1")
                return True, array
            else:
                # print("test2")
                return False, array
        else:
            # print("test3")
            array = []
        return 2, array
    except:
        # print("test4")
        return 2, array


def read_from_port(ser):
    global array, bool, robot_working, ready_protocol
    reading = ser.read(1)
    bool, array = receive(reading, array)
    print('reading = ', reading)
    if bool == True:
        # time.sleep(1)
        ready_protocol = True
        array = []
        robot_working = True
        print(ready_protocol)


def sys_pneumatic(ser, mode="close"):
    if mode == "open":
        command = 0xF1
        checksum = inv_hex(0xF1)
    else:
        command = 0xF2
        checksum = inv_hex(0xF2)
    ser.write(bytes([0xff]))
    time.sleep(0.03)
    ser.write(bytes([0xff]))
    time.sleep(0.03)
    ser.write(bytes([command]))
    time.sleep(0.03)
    ser.write(bytes([0x00]))
    time.sleep(0.03)
    ser.write(bytes([0x00]))
    time.sleep(0.03)
    ser.write(bytes([0x00]))
    time.sleep(0.03)
    ser.write(bytes([0x00]))
    time.sleep(0.03)
    ser.write(bytes([checksum]))
    time.sleep(0.03)


def string_send():

    global add_file,list_new,string_ready,list_string,via_point,add_file_via
    point_send = []

    if list_new != []:
        point_send = list_new
    elif list_new == [] and via_point != []:
        point_send = via_point
    if point_send != []:
        for i in range(6):
            if point_send[i] == 0 and i not in range(3, 6):
                send_string = '{},'.format(i + 1) + '1,' + '00%.4f' % abs(point_send[i])
                list_string.append(send_string)
            if 0 < abs(point_send[i]) < 10:
                if float(point_send[i]) >= float(0) and i < 3:  # 1-3 dc-stepper case: > 0
                    send_string = '{},'.format(i + 1) + '1,' + '00%.4f' % abs(point_send[i])
                    list_string.append(send_string)
                elif float(point_send[i]) <= float(0) and i < 2:  # 1-2 dc case: < 0
                    send_string = '{},'.format(i + 1) + '2,' + '00%.4f' % abs(point_send[i])
                    list_string.append(send_string)
                elif i in range(3, 6):  # 3-5 servo
                    send_string = '{},'.format(4) + '{},'.format(i - 2) + '00%.3f' % abs(point_send[i])
                    list_string.append(send_string)
            elif 10 <= abs(point_send[i]) < 100:
                if float(point_send[i]) >= 0 and i < 3:  # 1-3 dc-stepper case: > 0
                    send_string = '{},'.format(i + 1) + '1,' + '0%.4f' % abs(point_send[i])
                    list_string.append(send_string)
                elif float(point_send[i]) <= 0 and i < 2:  # 1-2 dc case: < 0
                    send_string = '{},'.format(i + 1) + '2,' + '0%.4f' % abs(point_send[i])
                    list_string.append(send_string)
                elif i in range(3, 6):  # 3-5 servo
                    send_string = '{},'.format(4) + '{},'.format(i - 2) + '0%.4f' % abs(point_send[i])
                    list_string.append(send_string)
            elif abs(point_send[i]) >= 100:
                if float(point_send[i]) >= 0 and i < 3:  # 1-3 dc-stepper case: > 0
                    send_string = '{},'.format(i + 1) + '1,' + '%.4f' % abs(point_send[i])
                    list_string.append(send_string)
                elif float(point_send[i]) <= 0 and i < 2:  # 1-2 dc case: < 0
                    send_string = '{},'.format(i + 1) + '2,' + '%.4f' % abs(point_send[i])
                    list_string.append(send_string)
                elif i in range(3, 6):  # 3-5 servo
                    send_string = '{},'.format(4) + '{},'.format(i - 2) + '%.4f' % abs(point_send[i])
                    list_string.append(send_string)

    if list_new != []:
        add_file = 0
    elif list_new == [] and via_point != []:
        for i in range(6):  # pop out first 6
            via_point.pop(0)
        add_file_via -= 1
        print('via left = ', via_point)

    print('list string in send =',list_string)
    point_send = ''

@TODO
เอาไปยัดใส่ฟังก์ชั่นเสิร์ชซะ
def search_warehouse(word):
    global warehouse
    found = difflib.get_close_matches(word, warehouse.values())
    if found:
        return found
    else:
        max_found = -1
        for slot in warehouse:
            if warehouse[slot] != "Null":
                n_found = 0
                cpy_word = word
                for char in warehouse[slot]:
                    if char in cpy_word:
                        n_found += 1
                        list_word = list(cpy_word)
                        list_word[cpy_word.find(char)] = ""
                        cpy_word = "".join(list_word)
                if n_found > max_found:
                    max_found = n_found
                    matchest = warehouse[slot]
                    matchest_slot = slot
        warehouse[matchest_slot] = "Null"
        @TODO
        return_ matchest word and warehouse slot
        return matchest, matchest_slot


def image_go(ser):
    global bool, ready_protocol, add_file, list_string, list_new, robot_working, via_point, warehouse, add_file_via,\
        take_data_timing

    pygame.init()
    width = 1280
    height = 720
    screen = pygame.display.set_mode((width, height))
    screen.fill((255, 255, 255))
    x_setting = 390  # control location some page don't change this
    y_setting = 400  # control location some page don't change this

    #  <------------------ variable for page 1(warehouse) lower ------------------>
    data_storage = []
    hold_data = []
    data_inside = []
    active = False
    #  <------------------ variable for page 1(warehouse) upper ------------------>

    #  <------------------ variable for page 2(control cartesian-joint space) lower ------------------>
    active_page_2 = [False, False, False, False, False, False, False, False, False, False, False, False]
    get_i = 0  # tab controller
    Cartesian = ['X :', 'Y :', 'Z :', 'Roll :', 'Pitch :', 'Yaw :']
    joint = ['q1 :', 'q2 :', 'q3 :', 'q4 :', 'q5 :', 'q6 :']
    cartesian_limit = [1057.5, -1057.5, 1057.5, -1057.5, 1044.4, -1044.4]
    joint_limit = [-135, 135, -90, 20, -60, 90, -90, 90, -90, 90, -90, 90]
    cartesian_data = ['', '', '', '', '', '']
    joint_data = ['', '', '', '', '', '']
    via_point_warehouse = {
        (1, 1): "Null",
        (1, 2): "Null",
        (1, 3): "Null",
        (2, 1): "Null",
        (2, 2): "Null",
        (2, 3): "Null",
        (3, 1): "Null",
        (3, 2): "Null",
        (3, 3): "Null",
        (3, 4): "Null",
        (4, 1): "Null",
        (4, 2): "Null",
        (4, 3): "Null",
        (4, 4): "Null"
    }
    q1 = [20,17]
    q2 = [-11,-38]
    q3 = [25+64,53+64]
    q4 = [0+90,0+90]
    q5 = [13+90,14+90]
    q6 = [0+90,72+90]
    via_index = [q1,q2,q3,q4,q5,q6]
    Articulated_df = pd.DataFrame(
        {'X': [''], 'Y': [''], 'Z': [''], 'Roll': [''], 'Pitch': [''], 'Yaw': [''], 'q1': [''], 'q2': [''], 'q3': [''],
         'q4': [''], 'q5': [''], 'q6': [''], 'type': ['']})
    joint_df = pd.DataFrame({'q1': [''], 'q2': [''], 'q3': [''], 'q4': [''], 'q5': [''], 'q6': ['']})
    list_index = [(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3), (3, 4), (4, 1),(4, 2), (4, 3), (4, 4)]
    #  <------------------ variable for page 2(control cartesian-joint space) upper ------------------>

    #  <------------------ variable for animation controller lower ------------------>
    change_page = 0
    old_page = 0
    time_current = 0
    rot = 0
    random_result = random.randint(0, 10)
    path = 'animation/'
    bg = pygame.image.load(path + 'bg.png')
    ani_01 = pygame.image.load(path + 'rushmore.png')
    ani_02 = pygame.image.load(path + 'emperor-penguinxd_v2.png')
    ani_03 = pygame.image.load(path + 'emperor-penguinxe_v2.png')
    #  <------------------ variable for animation controller upper ------------------>

    for index_number in range(len(q1)):
        for q in range(0,6):
            via_point.append(via_index[q][index_number])

    add_file_via = len(via_point)/6
    print(add_file_via)
    print(via_point)

    activate_pneumatic = ''
    comd_pneumatic = ''
    list_print = []
    text = ''  # text in every page
    page = 0  # current page
    count = 0
    first_time = 0

    cam = 0
    cap = cv2.VideoCapture(cam)
    if not cap.isOpened():
        raise IOError("Cannot open webcam")

    while True:
        if not robot_working:
            read_from_port(ser)

        _, frame = cap.read()
        if take_data_timing:
            @TODO
            ขนาดกล่องอยู่นี่
            cropped, box_type = utils.image_processing(frame)
            if np.any(cropped):
                cropped = cv2.resize(cropped, (640, 440))
                result = model.main(cropped)
                name = result["name"]
                room = result["room"]
                data = "{0} {1}".format(name, room)
                free_list = to_warehouse(data)
                if min(free_list) < 100:
                    for index_in_list in range(len(free_list)):
                        if free_list[i] == min(free_list):
                            via_point = via_point_warehouse[list_index[index_in_list]]
                            add_file_via = len(via_point) / 6
                            first_time = 0
                            count = 1
                            take_data_timing = False

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if cam == 1:
            frame = cv2.flip(frame, 1)  # for webcam camera
        frame = np.rot90(frame)  # pygame interface only!

        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:  # click event
                x_pos_click, y_pos_click = pygame.mouse.get_pos()
                if page == 0:
                    if x_pos_click in range(x_setting, 2 * x_setting + 110) and y_pos_click in range(y_setting,
                                                                                                     y_setting + 100):
                        old_page = page
                        change_page = 1
                        page = 1
                    elif x_pos_click in range(x_setting, 2 * x_setting + 110) and y_pos_click in range(y_setting + 150,
                                                                                                       y_setting + 250):
                        old_page = page
                        change_page = 1
                        page = 2
                elif page == 1:
                    active, page, hold_data, data_storage, text, data_inside, change_page, old_page = \
                        page_one_controller(active, page, x_pos_click, y_pos_click, hold_data, data_storage,
                                            data_inside, text, warehouse)
                elif page == 2:
                    active_page_2, text, cartesian_data, joint_data, page, change_page, old_page, joint_df, \
                    comd_pneumatic = page_two_controller(active_page_2, page, x_setting, y_setting, x_pos_click,
                                                         y_pos_click, cartesian_data, joint_data, text, Articulated_df,
                                                         joint_limit, cartesian_limit, joint_df, comd_pneumatic)
            if event.type == pygame.KEYDOWN:
                if page == 1:
                    if active:
                        # -----------text event----------#
                        if event.key == pygame.K_RETURN:  # enter?
                            data_storage = text
                            text = ''
                        elif event.key == pygame.K_BACKSPACE:
                            text = text[:-1]
                            data_storage = text
                        else:
                            text += event.unicode
                            data_storage = text
                        # -----------text event----------#
                if page == 2:
                    for i in range(len(active_page_2)):
                        if active_page_2[i]:
                            if i < 6:
                                # -----------text event----------#
                                if event.key == pygame.K_RETURN:
                                    cartesian_data[i] = text
                                    text = ''
                                elif event.key == pygame.K_BACKSPACE:
                                    text = text[:-1]
                                    cartesian_data[i] = text
                                else:
                                    if event.unicode in '.-0123456789':
                                        text += event.unicode
                                    cartesian_data[i] = text
                                # -----------text event----------#
                            else:
                                # -----------text event----------#
                                if event.key == pygame.K_RETURN:
                                    joint_data[i - 6] = text
                                    text = ''
                                elif event.key == pygame.K_BACKSPACE:
                                    text = text[:-1]
                                    joint_data[i - 6] = text
                                else:
                                    if event.unicode in '.-0123456789':
                                        text += event.unicode
                                    joint_data[i - 6] = text
                                # -----------text event----------#
                    if event.key == pygame.K_TAB:
                        for i in range(len(active_page_2)):
                            if active_page_2[i]:
                                get_i = i
                        text = ''
                        if get_i == 11:
                            active_page_2[get_i] = False
                            active_page_2[0] = True
                        else:
                            active_page_2[get_i] = False
                            active_page_2[get_i + 1] = True

        mouse = pygame.mouse.get_pos()
        spin_01 = pygame.transform.rotate(ani_02, rot)
        spin_02 = pygame.transform.rotate(ani_03, rot)
        # <------------------------------------ create table lower ------------------------------------------>
        blit_alpha(screen, bg, (0, 0), 255)
        frame = cv2.resize(frame, None, fx=0.7, fy=0.7, interpolation=cv2.INTER_LINEAR)
        frame = pygame.surfarray.make_surface(frame)
        blit_alpha(screen, frame, (416, 10), 255)

        if change_page == 1:
            time_current, change_page = animation(screen, time_current, ani_01, spin_01, spin_02, change_page, page,
                                                  mouse, x_setting, y_setting, Cartesian, cartesian_data, joint,
                                                  joint_data, joint_limit, cartesian_limit, old_page, random_result,
                                                  warehouse, data_storage)
        else:
            command_movepage(screen, page, mouse, x_setting, y_setting, Cartesian, cartesian_data, joint, joint_data,
                             joint_limit, cartesian_limit, warehouse, data_storage)
            time_current = 0
            random_result = random.randint(0, 10)

        rot += 20
        pygame.display.flip()
        # <------------------------------------ create table upper ------------------------------------------>

        if add_file_via == 0 and ready_protocol == False and robot_working == False and trigger_end_process == True:
            take_data_timing = True

        if add_file_via >= 1 :
            if first_time == 0:
                if count == 1:
                    string_send()
                    ready_protocol = False
                    trigger_end_process = False
                    count += 1
                    first_time = 1
            else:
                if ready_protocol == True:
                    string_send()
                    ready_protocol = False
                    trigger_end_process = False
                    count += 1

        if list_new != []:
            string_send()

        # 12 string
        if list_string != []:
            for i in list_string:
                string = str(i) + "\r\n"
                ser.write(bytes(string, 'utf-8'))
                robot_working = False
                time.sleep(0.5)
            trigger_end_process = True
            string = ''

            print('list_string after send =', list_string)
            list_new = []
            list_string = []

        if activate_pneumatic != comd_pneumatic:
            if comd_pneumatic == 'open':
                string = str('5,2,000.0000')
                ser.write(bytes(string, 'utf-8'))
            elif comd_pneumatic == 'close':
                string = str('5,1,000.0000')
                ser.write(bytes(string, 'utf-8'))
            elif comd_pneumatic == 'via_on':
                robot_working = False
                count = 1
            elif comd_pneumatic == 'via_off':
                robot_working = True
            elif comd_pneumatic == 'zero':
                robot_working = False
                string = str('0,0,000.0000')
                ser.write(bytes(string, 'utf-8'))

            string = ''
            comd_pneumatic = ''
            activate_pneumatic = ''

    ser.close()

event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, path='communicating/', recursive=False)
observer.start()

# <--------------- protocal data ------------------->
# ser = serial.Serial()
# ser.baudrate = 9600
# ser.port = 'COM10'
# ser.timeout = 1
# ser.dtr = 0
# ser.rts = 0
# ser.open()
# test = []

# ser = 0 # For not use
ready_protocol = False
# <--------------- protocal data ------------------->

# t2 = threading.Thread(target=read_from_port, args=(ser,))
# t2.setDaemon(True)
t1 = threading.Thread(target=image_go(ser))
t1.setDaemon(True)
# t2.start()
t1.start()
while True:
    pass
