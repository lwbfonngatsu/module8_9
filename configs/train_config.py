import os
import categories

agent = "num"

switch = {
    "lower": categories.label_lower,
    "mid" : categories.label_mid,
    "num" : categories.label_num,
    "upper": categories.label_upper,
    "integizer": categories.label_integizer
}
classes = list(switch.get(agent).keys())

agent_path = "Models/CNN/{}_model".format(agent)

if not os.path.isdir(agent_path):
    os.makedirs(agent_path)

train_images_path = r"Dataset_v2\{}\_train".format(agent)
valid_images_path = r"Dataset_v2\{}\_valid".format(agent)
test_images_path  = r"Dataset_v2\{}\_test".format(agent)

train_csv_path = r"Dataset\{}\_train\annotation.csv".format(agent)
valid_csv_path = r"Dataset\{}\_valid\annotation.csv".format(agent)
test_csv_path  = r"Dataset\{}\_test\annotation.csv".format(agent)

train = 1

cnn_version = len(os.listdir(agent_path))

folder_path = agent_path + "/Model_v{0}".format(cnn_version)
if not os.path.isdir(folder_path):
    os.makedirs(folder_path)
json_path = folder_path + "/arch.json"
model_path = folder_path + "/model.hdf5"
text_path = folder_path + "/info.txt"
