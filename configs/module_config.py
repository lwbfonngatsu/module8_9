import categories

port = "COM3"
baudrate = 9600

upper_models = [0, 3, 4, 5]
mid_models = [19, 23, 25, 34]
num_models = [1]
lower_models = [0, 1]
integizer_models = [5]

switch = {
    "lower": categories.label_lower,
    "mid": categories.label_mid,
    "num": categories.label_num,
    "upper": categories.label_upper,
    "integizer": categories.label_integizer
}
low_cls = list(switch.get("lower").keys())
mid_cls = list(switch.get("mid").keys())
num_cls = list(switch.get("num").keys())
upp_cls = list(switch.get("upper").keys())
int_cls = list(switch.get("integizer").keys())

low_label = switch.get("lower")
mid_label = switch.get("mid")
num_label = switch.get("num")
upp_label = switch.get("upper")
int_label = switch.get("integizer")
