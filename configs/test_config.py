import os
import categories

agent = "integizer"

switch = {
    "lower": categories.label_lower,
    "mid" : categories.label_mid,
    "num" : categories.label_num,
    "upper": categories.label_upper,
    "integizer": categories.label_integizer
}
classes = list(switch.get(agent).keys())

agent_path = "Models/CNN/{}_model".format(agent)

train_images_path = "Dataset\{}\_train".format(agent)
valid_images_path = "Dataset\{}\_valid".format(agent)
test_images_path  = "Test_image\Crop_hand\{}".format(agent)

cnn_version = len(os.listdir(agent_path))

folder_path = agent_path + "/Model_v{0}".format(cnn_version)

json_path = folder_path + "/arch.json"
model_path = folder_path + "/model.hdf5"
text_path = folder_path + "/info.txt"
