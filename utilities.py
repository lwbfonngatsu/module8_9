from PIL import Image, ImageDraw, ImageFont
import cv2
import os
import numpy as np
from skimage.feature import hog
from skimage.util import img_as_ubyte
from skimage import morphology, img_as_bool
import configs.module_config as config


def text2img(text, color="#000", bgcolor="#FFF",
             fontfullpath=None, fontsize=300, leftpadding=50, rightpadding=50, width=1000):
    REPLACEMENT_CHARACTER = u'\uFFFD'
    NEWLINE_REPLACEMENT_STRING = ' ' + REPLACEMENT_CHARACTER + ' '
    font = ImageFont.load_default() if fontfullpath is None else ImageFont.truetype(fontfullpath, fontsize)
    text = text.replace('\n', NEWLINE_REPLACEMENT_STRING)
    lines = []
    line = u""
    for word in text.split():
        if word == REPLACEMENT_CHARACTER:
            lines.append(line[1:])
            line = u""
            lines.append(u"")
        elif font.getsize(line + ' ' + word)[0] <= (width - rightpadding - leftpadding):
            line += ' ' + word
        else:
            lines.append(line[1:])
            line = u""
            line += ' ' + word
    if len(line) != 0:
        lines.append(line[1:])
    line_height = font.getsize(text)[1]
    img_height = line_height * (len(lines) + 1)
    img = Image.new("RGBA", (width, img_height), bgcolor)
    draw = ImageDraw.Draw(img)
    y = 0
    for line in lines:
        draw.text((leftpadding, y), "   " + line, color, font=font)
        y += line_height
    return np.asarray(img)


def udt2img(text, fontfullpath, fontsize=300):
    img = Image.new('RGB', (500, 500), 'white')
    font = ImageFont.load_default() if fontfullpath is None else ImageFont.truetype(fontfullpath, fontsize)
    draw = ImageDraw.Draw(img)
    draw.text((50, 50), text, (0, 0, 0), font=font)
    open_cv_image = np.array(img)
    image = open_cv_image[:, :, ::-1].copy()
    return image


def find_bndbox(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    retval, thresh_gray = cv2.threshold(gray, thresh=100, maxval=255, type=cv2.THRESH_BINARY)
    points = np.argwhere(thresh_gray == 0)
    points = np.fliplr(points)
    return cv2.boundingRect(points)


def crop_letter(fullpath="", img=None):
    if np.any(img) is not None:
        x, y, w, h = find_bndbox(img)
        crop = img[y - 2:y + 2 + h, x - 2:x + 2 + w]
        return crop
    else:
        if not os.path.isdir(fullpath):
            image = cv2.imread(fullpath)
            x, y, w, h = bnd_box = find_bndbox(image)
            print("Cropping", fullpath, bnd_box)
            crop = image[y + 2:y + 2 + h, x + 2:x + 2 + w]
            cv2.imwrite(fullpath + "/cropped_" + fullpath, crop)
        else:
            paths = os.listdir(fullpath)
            for path in paths:
                if path.split(".")[-1] == "png":
                    image = cv2.imread(fullpath + path)
                    x, y, w, h = bnd_box = find_bndbox(image)
                    print("Cropping", path, bnd_box)
                    crop = image[y + 2:y + 2 + h, x + 2:x + 2 + w]
                    cv2.imwrite(fullpath + "/cropped_" + path, crop)


def ratio_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    (h, w) = image.shape[:2]
    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))
    resized = cv2.resize(image, dim, interpolation=inter)
    return resized


def save_image(img, name, path):
    index = 0
    if not os.path.isdir(path):
        os.makedirs(path)
    if isinstance(img, (list,)):
        for image in img:
            cv2.imwrite(path + name[index], image)
            index += 1
    else:
        cv2.imwrite(path + name, img)


def skeletonize(img):
    image = img_as_bool(img)
    out = morphology.medial_axis(image)
    return img_as_ubyte(out)


def preprocess(image, generating=False):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(image, 155, 255, cv2.THRESH_BINARY)
    skeletonized = skeletonize(255 - thresh)
    _, hog_img = hog(skeletonized, orientations=32, pixels_per_cell=(2, 2),
                               cells_per_block=(1, 1), visualise=True)
    if not generating:
        hog_img = np.repeat(hog_img[..., np.newaxis], 3, -1)
    return hog_img


prev_hue = 0
steady_counter = 0


def adjust_gamma(image, gamma=1.0):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)


def image_normalizer(image, inverse=False, gamma=1.0):
    adjusted_image = adjust_gamma(cv2.cvtColor(image, cv2.COLOR_BGR2HSV), gamma)
    if inverse:
        hsv = adjusted_image
        hsv[..., 2] = 255 - adjusted_image[..., 2]
        hsv = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    else:
        hsv = cv2.cvtColor(adjusted_image, cv2.COLOR_HSV2BGR)

    hist, bins = np.histogram(hsv.flatten(), 256, [0, 256])
    cdf = hist.cumsum()
    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
    cdf = np.ma.filled(cdf_m, 0).astype('uint8')
    normalized_image = cdf[hsv]

    return normalized_image


def steady_correction(hue, debug):
    global steady_counter, prev_hue
    if debug:
        print("previous hue:", prev_hue, "present hue:", hue)
        print("Steadying:", steady_counter)
    if abs(prev_hue - hue) > 10000:
        steady_counter = 0
        return False
    else:
        steady_counter += 1
        return True


def image_processing(input, debug=False):
    global prev_hue
    type_box = None
    max_area = 90000
    min_area = 60000
    lower_white = np.array([0, 0, 0], dtype=np.uint8)
    upper_white = np.array([0, 0, 255], dtype=np.uint8)
    cpy_frame = input.copy()
    input[: 80, 0: input.shape[1]] = 0
    input[input.shape[0] - 50: input.shape[0], 0: input.shape[1]] = 0
    input[: input.shape[0], 0: 30] = 0
    hsv = cv2.cvtColor(input, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)
    h[h > 0] = 0
    s[s > 128] = 255
    s[s < 128] = 0
    steady_correction(np.count_nonzero(s), debug)
    prev_hue = np.count_nonzero(s)
    if steady_counter >= 100:
        crop = None
        if prev_hue > 120000:
            final_hsv = cv2.merge((h, s, v))
            mask = cv2.inRange(final_hsv, lower_white, upper_white)
            res = cv2.bitwise_and(input, input, mask=mask)
            _, thresh = cv2.threshold(res, 0, 255, cv2.THRESH_BINARY)
            kernel = np.ones((5, 5), np.uint8)
            closing = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
            closing = cv2.cvtColor(closing, cv2.COLOR_BGR2GRAY)
            _, contours, _ = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) != 0:
                largestContourArea = 0
                for cnt in contours:
                    contourArea = cv2.contourArea(cnt)
                    _, _, _w, _h = cv2.boundingRect(cnt)
                    if contourArea > largestContourArea and max_area > _w * _h > min_area:
                        largestContourArea = contourArea
                        x, y, w, h = cv2.boundingRect(cnt)
                        crop = input[y: y + h, x: x + w]
                type_box = 1
        elif prev_hue > 40000:
            cpy_frame[: 120, : cpy_frame.shape[1]] = 0
            cpy_frame[cpy_frame.shape[0] - 60: cpy_frame.shape[0], 0: cpy_frame.shape[1]] = 0

            cpy_frame[: cpy_frame.shape[0], : 150] = 0
            cpy_frame[: cpy_frame.shape[0], cpy_frame.shape[1] - 150:] = 0
            cv2.imshow("sdasdasda", cpy_frame)
            cv2.waitKey(0)
            gray = cv2.cvtColor(cpy_frame, cv2.COLOR_BGR2GRAY)
            _, thresh = cv2.threshold(gray, 96, 255, cv2.THRESH_BINARY)
            kernel = np.ones((9, 9), np.uint8)
            opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
            _, contours, _ = cv2.findContours(opening, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) != 0:
                largestContourArea = 0
                for cnt in contours:
                    contourArea = cv2.contourArea(cnt)
                    _, _, _w, _h = cv2.boundingRect(cnt)
                    if contourArea > largestContourArea and max_area > _w * _h > min_area:
                        largestContourArea = contourArea
                        x, y, w, h = cv2.boundingRect(cnt)
                        crop = input[y: y + h, x: x + w]
                type_box = 0
        else:
            cpy_frame[: 80, 0: cpy_frame.shape[1]] = 255
            cpy_frame[cpy_frame.shape[0] - 60: cpy_frame.shape[0], 0: cpy_frame.shape[1]] = 255
            cpy_frame[: cpy_frame.shape[0], 0: 30] = 255
            gray = cv2.cvtColor(cpy_frame, cv2.COLOR_BGR2GRAY)
            _, thresh = cv2.threshold(gray, 96, 255, cv2.THRESH_BINARY)
            thresh = 255 - thresh
            kernel = np.ones((2, 2), np.uint8)
            opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
            kernel = np.ones((100, 100), np.uint8)
            dilate = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel)
            _, contours, _ = cv2.findContours(dilate, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) != 0:
                largestContourArea = 0
                for cnt in contours:
                    contourArea = cv2.contourArea(cnt)
                    _, _, _w, _h = cv2.boundingRect(cnt)
                    if contourArea > largestContourArea and max_area > _w * _h > min_area:
                        largestContourArea = contourArea
                        x, y, w, h = cv2.boundingRect(cnt)
                        crop = input[y: y + h, x: x + w]
                type_box = 1
        if debug:
            cv2.drawContours(thresh, contours, -1, (0, 255, 0), 3)
            cv2.imshow("thresh", thresh)
            cv2.imshow("input", input)
            if np.any(crop):
                cv2.imshow("crop", crop)
        else:
            if np.any(crop):
                return crop, type_box
            else:
                return None, None


if __name__ == "__main__":
    cap = cv2.VideoCapture(0)
    while True:
        _, frame = cap.read()
        cropped = image_processing(frame, debug=True)
        if np.any(cropped):
            cv2.imshow("crop", cropped)
            cv2.imwrite("crop2.jpg", cropped)
        cv2.waitKey(1)
